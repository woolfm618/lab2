#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>

#define M_PI 3.14159265358979323846

class Tracktriss {

private:                                                   // ����� 1
	double track_const = 0;

public:
	double x1 = 0;
	double y1 = 0;

private:
	void Round_Double(int after_dot) {
		x1 = round(x1 * pow(10, after_dot)) / pow(10, after_dot);
		y1 = round(y1 * pow(10, after_dot)) / pow(10, after_dot);
	}
	double Round_Double(double target_double, int after_dot) {
		return round(target_double * pow(10, after_dot)) / pow(10, after_dot);
	}

public:
	Tracktriss() {}                                       // ����������� ���������

	Tracktriss(double a_const) {
		Change_Params(a_const);
	}

	void Change_Params(double a_const){                    // ����� 2, ����������� �� ���������� 
		Tracktriss::track_const = a_const;
	}     

	void Dot_on_PM(double turn_angle) {                    // ����� 3 ���������� ����� �� PM 
		x1 = -track_const * (log(tan((M_PI - turn_angle) / 2.)) + cos(M_PI - turn_angle));
		y1 = track_const * sin(M_PI - turn_angle);
		Round_Double(10);
	}                     
	double Arc_Length(float turn_angle) {                      // ����� 4 ������ ���� 
		return abs( track_const * turn_angle);
	}                   
	double Arc_Curvature(float turn_angle) {                  // ����� 5 ������ �������� 
		return Round_Double(abs(track_const * (1 / tan(M_PI - turn_angle))), 10);
	}                   
	double Space() {                                         // ����� 6 ������� ����� ����� � ����������
		return abs(M_PI * pow(track_const, 2) / 2);
	}                                        
	double Body_Surface() {                                 // ����� 7 ������� ����������� ���� �������� (�����������)
		return abs(4 * M_PI * pow(track_const, 2));
	}                                
	double Body_Volume() {                                  // ����� 8 ����� ���� �������� (�����������)
		if (track_const == 0) {
			return 0;
		}
		return abs(2 /( 3. * M_PI * pow(track_const, 3)));
	}
};