#include "pch.h"
#include"D:\�����\3 ���\�����\2� ����\Lab2Sem3\Lab2Sem3\Tracktriss.h"

TEST(TestInitBasic, InitTest) {         // �� ���� �������������� ��������� ����_����� ��� ��� ��� ��������� 
	Tracktriss tracktriss;
	EXPECT_DOUBLE_EQ(tracktriss.x1, 0);
	EXPECT_DOUBLE_EQ(tracktriss.y1, 0);
	EXPECT_HRESULT_SUCCEEDED(tracktriss.x1);
	EXPECT_HRESULT_SUCCEEDED(tracktriss.y1);
}
TEST(TestInitCustom, InitTest) {
	Tracktriss tracktriss(3);
	EXPECT_DOUBLE_EQ(tracktriss.x1, 0);
	EXPECT_DOUBLE_EQ(tracktriss.y1, 0);
	EXPECT_HRESULT_SUCCEEDED(tracktriss.x1);
	EXPECT_HRESULT_SUCCEEDED(tracktriss.y1);
}

TEST(TestDot1_1,DotTest) {
	Tracktriss tracktriss(1);
	tracktriss.Dot_on_PM(3.14);
	EXPECT_DOUBLE_EQ(tracktriss.x1, 6.1355019669); //round(tracktriss.x1 * pow(10, 10)) / pow(10, 10)
	EXPECT_DOUBLE_EQ(tracktriss.y1, 0.0015926529);
}
TEST(TestDot1_2, DotTest) {
	Tracktriss tracktriss(1);
	tracktriss.Dot_on_PM(0.02);
	EXPECT_DOUBLE_EQ(tracktriss.x1,-3.6053368452);
	EXPECT_DOUBLE_EQ(tracktriss.y1, 0.0199986667);
}
TEST(TestDot2_1, DotTest) {
	Tracktriss tracktriss(-1);
	tracktriss.Dot_on_PM(3.14);
	EXPECT_DOUBLE_EQ(tracktriss.x1, -6.1355019669);
	EXPECT_DOUBLE_EQ(tracktriss.y1, -0.0015926529);
}
TEST(TestDot2_2, DotTest) {
	Tracktriss tracktriss(-1);
	tracktriss.Dot_on_PM(0.01);
	EXPECT_DOUBLE_EQ(tracktriss.x1, 4.2983590327);
	EXPECT_DOUBLE_EQ(tracktriss.y1, -0.0099998333);
}
TEST(TestDot3_1, DotTest) {
	Tracktriss tracktriss(0);
	tracktriss.Dot_on_PM(1);
	EXPECT_DOUBLE_EQ(tracktriss.x1, 0);
	EXPECT_DOUBLE_EQ(tracktriss.y1, 0);
}


TEST(Test_Arc_Length_1, Arc_Length_Test) {
	Tracktriss tracktriss(0);
	tracktriss.Arc_Length(0);
	EXPECT_DOUBLE_EQ(tracktriss.x1, 0);
}  
TEST(Test_Arc_Length_2, Arc_Length_Test) {
	Tracktriss tracktriss(1);
	EXPECT_DOUBLE_EQ(tracktriss.Arc_Length(1) , 1);
}
TEST(Test_Arc_Length_3, Arc_Length_Test) {
	Tracktriss tracktriss(1);
	EXPECT_DOUBLE_EQ(tracktriss.Arc_Length(2) , 2);
}

TEST(Test_Arc_Curvature_1, Arc_Curvature_Test) {
	Tracktriss tracktriss(1);
	EXPECT_DOUBLE_EQ(tracktriss.Arc_Curvature(1.5), 0.0709148443);
}
TEST(Test_Arc_Curvature_3, Arc_Curvature_Test) {
	Tracktriss tracktriss(0);
	EXPECT_DOUBLE_EQ(tracktriss.Arc_Curvature(1.5), 0.);
}
TEST(Test_Arc_Curvature_4, Arc_Curvature_Test) {
	Tracktriss tracktriss(1);
	EXPECT_DOUBLE_EQ(tracktriss.Arc_Curvature(0.5), 1.8304877217);
}
TEST(Test_Arc_Curvature_5, Arc_Curvature_Test) {
	Tracktriss tracktriss(-1);
	EXPECT_DOUBLE_EQ(tracktriss.Arc_Curvature(1.5), 0.0709148443);
}


TEST(Test_Space_1, Space_Test) {
	Tracktriss tracktriss(1);
	EXPECT_DOUBLE_EQ(tracktriss.Space(), 1.5707963267948966);
}
TEST(Test_Space_2, Space_Test) {
	Tracktriss tracktriss(0);
	EXPECT_DOUBLE_EQ(tracktriss.Space(),0.);
}
TEST(Test_Space_3, Space_Test) {
	Tracktriss tracktriss(-1);
	EXPECT_DOUBLE_EQ(tracktriss.Space(), 1.5707963267948966);
}


TEST(Test_Surface_1, Surface_Test) {
	Tracktriss tracktriss(1);
	EXPECT_DOUBLE_EQ(tracktriss.Body_Surface(), 12.566370614359172);
}
TEST(Test_Surface_2, Surface_Test) {
	Tracktriss tracktriss(-1);
	EXPECT_DOUBLE_EQ(tracktriss.Body_Surface(), 12.566370614359172);
}
TEST(Test_Surface_3, Surface_Test) {
	Tracktriss tracktriss(0);
	EXPECT_DOUBLE_EQ(tracktriss.Body_Surface(), 0);
}


TEST(Test_Volume_1, Volume_Test) {
	Tracktriss tracktriss(1);
	EXPECT_DOUBLE_EQ(tracktriss.Body_Volume(), 0.2122065907891938);
}
TEST(Test_Volume_2, Volume_Test) {
	Tracktriss tracktriss(-1);
	EXPECT_DOUBLE_EQ(tracktriss.Body_Volume(), 0.2122065907891938);
}
TEST(Test_Volume_3, Volume_Test) {
	Tracktriss tracktriss(0);
	EXPECT_DOUBLE_EQ(tracktriss.Body_Volume(), 0.);
}