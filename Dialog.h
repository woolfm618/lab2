#include <iostream>
#include "Tracktriss.h"

class Dialog {
private:
	int choise = 0;
	double tmp1 = 0;
	double angle = 0;

private:
	
	template  <class T>
	T InputCheck() {
		T number = 0;
		std::cin >> number;
		while (std::cin.fail()) {
			std::cin.clear();     // ����� ��������� �������� ����� ���� ����� ���� ������ �������� 
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // � ������� ��� ����������� � ��� ������
			std::cout << "Incorrect! Repeat Input:";
			std::cin >> number;
		}
		return number;
	}

	double Angle_Check() {
		angle = InputCheck<double>();
		while (angle >= M_PI || angle <= 0) {
			std::cout << "Angle should be in (0;pi) Repeat input:";
			angle = InputCheck<double>();
		}
		return angle;
	}

public:
	void Run_dialog() {
		std::cout << "tracktriss const:";
		tmp1 = InputCheck<double>();
		Tracktriss tracktriss(tmp1);

		while (1) {
			std::cout << "1. find touch point\n2. touch point arc length\n3. tractriss curvature\n4. tracktriss space\n5. body surface\n6. body volume\n0. exit\n";

			choise = InputCheck<int>();

			if (choise == 1) {
				std::cout << "tangent angle:";
				tmp1 = Angle_Check();
				tracktriss.Dot_on_PM(tmp1);
				std::cout << "x, y = " << tracktriss.x1 << " " << tracktriss.y1 << "\n";
			}
			if (choise == 2) {
				std::cout << "tangent angle:";
				tmp1 = Angle_Check();
				std::cout << "arc length: " << tracktriss.Arc_Length(tmp1) << "\n";
			}
			if (choise == 3) {
				std::cout << "tangent angle:";
				tmp1 = Angle_Check();
				std::cout << "curvature: " << tracktriss.Arc_Curvature(tmp1) << "\n";
			}
			if (choise == 4) {
				std::cout << "S = " << tracktriss.Space() << "\n";
			}
			if (choise == 5) {
				std::cout << "S = " << tracktriss.Body_Surface() << "\n";
			}
			if (choise == 6) {
				std::cout << "V = " << tracktriss.Body_Volume() << "\n";
			}
			if (choise == 0) {
				break;
			}
			else {
				continue;
			}
		}
	}
};
